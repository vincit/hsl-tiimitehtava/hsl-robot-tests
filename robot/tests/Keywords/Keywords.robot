*** Settings ***
Library       SeleniumLibrary
Library       BuiltIn
Library       RequestsLibrary
Library       OperatingSystem
Library       Collections
Library       String
Library       DateTime
Resource      ./test_setup_and_teardownd_keywords.robot
Resource      ./given.robot
Resource      ./when.robot
Resource      ./then.robot
