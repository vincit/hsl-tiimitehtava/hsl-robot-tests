*** Keywords ***
user is on main page
    Location Should Be      ${FRONT_END_URL}
    Page Should Contain Element    ${APP_HEADER}

user opens back end page
    Go To   ${BACK_END_URL}
    Location Should Be    ${BACK_END_URL}
