*** Keywords ***
take screenshot
    Capture Page Screenshot

db connection should be alive
    Wait Until Page Contains     "dbConnectionAlive":true

time should be shown
    ${date_time}    Get Current Date
    ${date_time_list}    Split String    ${date_time}
    ${current_date}    Get From List    ${date_time_list}   0
    Page Should Contain    ${current_date}

status message should be shown
    Wait Until Page Contains    ${STATUS_MESSAGE}

#xml should be uploaded
