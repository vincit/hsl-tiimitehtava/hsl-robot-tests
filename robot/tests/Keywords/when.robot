*** Keywords ***
user selects xml file
    Wait Until Page Contains Element     ${CHOOSE_FILE_BUTTON}
    Element Should Be Disabled     ${UPLOAD_BUTTON}
    Choose File     ${CHOOSE_FILE_BUTTON}    ${PATH_TO_XML}
    Wait Until Element Is Enabled     ${UPLOAD_BUTTON}

user presses upload button
    Wait Until Element Is Enabled     ${UPLOAD_BUTTON}
    Click Element    ${UPLOAD_BUTTON}
