*** Settings ***
Resource        ./TestResources.robot
Force Tags      smoke
Test Setup       setup browser
Test Teardown    test teardown

***Test Cases***
Front Page Should Load
    Given user is on main page
    Then take screenshot

Back End Page Should Load
    Given user opens back end page
    Then db connection should be alive
    And time should be shown
