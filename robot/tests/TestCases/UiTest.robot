*** Settings ***
Resource        ./TestResources.robot
Force Tags       ui
Test Setup       setup browser
Test Teardown    test teardown

*** Variables ***
${STATUS_MESSAGE}   Something went wrong
*** Test Cases ***
User Can Upload XML File On Page
    Given user is on main page
    When user selects xml file
    And user presses upload button
    Then status message should be shown
    #And xml should be uploaded
